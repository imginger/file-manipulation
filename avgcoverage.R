#use the .plotcov output file from VanBakel pipeline to calculate average coverage per flu genome then output table to sample.txt file in current directory

file1 <- list.files(path = ".", pattern = "\\.plotcov")
input <- read.delim(file1, header=FALSE)
readaveragecalc <- sum(input$V3)/length(input$V3)
readaverage <- trunc(readaveragecalc)
sample <- getwd()
table <- data.frame(sample, readaverage)
write.csv(table, "sample.txt")  
avgcoverage.R (END) 
