#!/usr/bin/perl

#set variable to open any .fa file dynamically as input###
my @files = <*.fa>;
 for $file (@files) {

open (INFILE, "$file")    
or die "Can't open file";
while (<INFILE>) {
$line = $_;
chomp $line;
if ($line =~ /\>/) { #if has fasta >
close OUTFILE;
$new_file = substr($line,1);
$new_file .= ".fa";
open (OUTFILE, ">$new_file")
or die "Can't open: $new_file $!";
}
print OUTFILE "$line\n";
}
close OUTFILE;
#This perl script will break a concatenated fasta into its component fasta files and name the output according to the text after ">" in the fasta files. 